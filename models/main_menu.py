from odoo import api, fields, models,_


class Main_menu(models.Model):
	

	_name="main.menu"
	
	name = fields.Char('Name')
	model = fields.Many2one('ir.model','Model')
	custom_fields = fields.Many2many('ir.model.fields',string='Fields')
	status = fields.Selection([('add','Added'),('remove','Removed')],'Status')
	model_list = fields.Char('Model List')
	ref_ir_act_window_id = fields.Many2one('ir.actions.act_window',
										   'Sidebar action',
										   readonly=True,
										   help="Sidebar action to make this "
												"template available on "
												"records of the related "
												"document model.")
	@api.multi
	def add_server_button(self):
		vals={}
		button_name = self.name
		model_id = self.model.id
		fields =  self.custom_fields.ids
		# view = self.env.ref('imc.custom_wizard_form')

		# exists = self.env['ir.actions.act_window'].search([('name','=',button_name)])
		# if not exists:
		vals['ref_ir_act_window_id'] = self.env['ir.actions.act_window'].sudo().create({
			'name': button_name,
			'type': 'ir.actions.act_window',
			'res_model': 'fields.wizard',
			'src_model': self.model.model,
			'view_type': 'form',
			# 'view_id': view.id,
			'view_mode': 'form, tree',
			'target': 'new',
			'binding_model_id': self.model.id,
			'context': "{'mass_editing_object' : %d}" % (self.id),
		}).id
		self.write(vals)


	@api.onchange('model')
	def _onchange_model_id(self):
		self.field_ids = [(6, 0, [])]
		model_list = []
		if self.model:
			model_obj = self.env['ir.model']
			model_list = [self.model.id]
			active_model_obj = self.env[self.model.model]
			if active_model_obj._inherits:
				keys = active_model_obj._inherits.keys()
				inherits_model_list = model_obj.search([('model', 'in', keys)])
				model_list.extend((inherits_model_list and
								   inherits_model_list.ids or []))
		self.model_list = model_list


	@api.multi
	def remove_server_button(self):
		self.mapped('ref_ir_act_window_id').sudo().unlink()
		return True

	@api.multi
	def unlink(self):
		self.remove_server_button()
		return super(Main_menu, self).unlink()

	@api.returns('self', lambda value: value.id)
	def copy(self, default=None):
		if default is None:
			default = {}
		default.update({'name': _("%s (copy)" % self.name), 'field_ids': []})
		return super(Main_menu, self).copy(default)


Main_menu()

