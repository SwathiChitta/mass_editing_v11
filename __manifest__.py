{
	'name': 'Mass Editing',
	'version': '11.0',
	'author': 'Anipr Technologies',
	'category': 'Tools',
	'website': 'http://www.anipr.com',
	'license': 'GPL-3 or any later version',
	'summary': 'Mass Editing',
	'depends': ['base'],
	'data': [
		'views/main_menu.xml',
	],
	'installable': True,
	'application': False,
	'auto_install': False,
}
